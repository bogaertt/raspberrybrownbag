import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PhotoComparatorTest {

    @Test
    public void testCalculateDifference() throws Exception {
        String referenceFile = getClass().getResource("/globe_and_high_court_orig.jpg").getFile();
        String currentFile = getClass().getResource("/globe_and_high_court_update1.jpg").getFile();

        PhotoComparator photoComparator = new PhotoComparator(referenceFile, currentFile);
        assertEquals(12.64, photoComparator.calculateDifference().getPercentage(), 0.01);
    }
}
