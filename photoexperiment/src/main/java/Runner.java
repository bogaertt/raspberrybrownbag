import photo.Photo;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Kies invoerprentjes Bepaal hoeveel prentjes ->nbImagesInPicture en welke prentjes getoond moeten worden ->
 * paintComponent
 * <p/>
 * TODO: mask : image with same size with black and white with areas that can be ignored TODO: defining a 'score': ratio
 * of pixels that are 'different', in the area defined by the mask.
 */
public class Runner
{

    public static void main(String[] args)
    {
        new Runner();
    }

    public Runner()
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

                    JFrame frame = new JFrame("Photo differences");
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                    //frame.add(new PhotoComparator("C:/Users/jpellegrims/Desktop/testi.jpg", "C:/Users/jpellegrims/Desktop/test2.jpg", "C:/Users/jpellegrims/Desktop/allesmask.jpg"));
                    //frame.add(new PhotoComparator("C:/Users/jpellegrims/Desktop/kok1.jpg","C:/Users/jpellegrims/Desktop/kok2.jpg", "C:/Users/jpellegrims/Desktop/allesmask.jpg"));
                    frame.add(new PhotoComparator("C:/Users/jpellegrims/Desktop/monet1.jpg","C:/Users/jpellegrims/Desktop/monet2.jpg", "C:/Users/jpellegrims/Desktop/allesmask.jpg"));

                    frame.pack();
                    frame.setLocationRelativeTo(null);
                    frame.setVisible(true);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                //frame.add(new PhotoComparator("C:/Users/jpellegrims/Desktop/kok1.jpg","C:/Users/jpellegrims/Desktop/kok2.jpg", "C:/Users/jpellegrims/Desktop/allesmask.jpg"));
                //frame.add(new PhotoComparator("C:/Users/jpellegrims/Desktop/monet1.jpg","C:/Users/jpellegrims/Desktop/monet2.jpg"));
            }
        }
        );
    }

    public class PhotoComparator extends JPanel
    {
        private Photo reference;
        private Photo referenceGrey;
        private Photo current;
        private Photo currentGrey;
        private Photo difference;
        private Photo calibratedCurrent;
        private Photo finalDifference;
        private Photo mask;

        final int nbImagesInPicture = 4; //change me!

        public PhotoComparator(String referenceName, String currentname, String maskname) throws IOException
        {
            mask = new Photo(ImageIO.read(new File(maskname)));
            reference = new Photo(ImageIO.read(new File(referenceName)));
            current = new Photo(ImageIO.read(new File(currentname)));

            //referenceGrey = reference.grey();
            //currentGrey = current.grey();
            difference = current.substract(reference);
            calibratedCurrent = current.calibrate(reference, 10, 10, 0, 0);
            finalDifference = calibratedCurrent.difference(reference).grey();//.cutoff(80);
            //System.out.println("maskedDiff: "+finalDifference.calculateDiff(mask));
            System.out.println("fullDiff: "+finalDifference.calculateDiff());

            Graphics2D g2d = current.image.createGraphics();
            g2d.drawImage(current.image, 0, 0, this);
            g2d.dispose();
        }


        @Override
        public Dimension getPreferredSize()
        {
            Dimension size = super.getPreferredSize();
            if (reference != null)
            {
                size = new Dimension(reference.image.getWidth() * nbImagesInPicture, reference.image.getHeight());
            }
            return size;
        }


        @Override
        protected void paintComponent(Graphics g)
        {
            Photo photo1 = reference;
            Photo photo2 = current;
            Photo photo3 = calibratedCurrent;
            Photo photo4 = finalDifference;

            super.paintComponent(g);
            if (reference != null)
            {

                int x = (getWidth() - (photo1.image.getWidth() * nbImagesInPicture)) / 2;
                int y = (getHeight() - photo1.image.getHeight()) / 2;

                g.drawImage(photo1.image, x, y, this);

                x += photo1.image.getWidth();
                g.drawImage(photo2.image, x, y, this);

                x += photo1.image.getWidth();
                g.drawImage(photo3.image, x, y, this);

                x += photo1.image.getWidth();
                g.drawImage(photo4.image, x, y, this);
            }
        }


    }
}