import photo.Photo;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class PhotoComparator {

    private Photo mask;
    private Photo reference;
    private Photo current;

    public PhotoComparator(String referenceFile, String currentFile) throws IOException {
        reference = new Photo(ImageIO.read(new File(referenceFile)));
        current = new Photo(ImageIO.read(new File(currentFile)));
    }

    public PhotoComparator(String referenceFile, String currentFile, String maskFile) throws IOException {
        this(referenceFile, currentFile);
        mask = new Photo(ImageIO.read(new File(maskFile)));
    }

    public Difference calculateDifference() throws IOException {
        Photo calibratedCurrent = current.calibrate(reference, 10, 10, 0, 0);
        Photo difference = calibratedCurrent.difference(reference).grey().cutoff(20);

        byte[] differenceBytes = null;
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();) {
            ImageIO.write(difference.image, ImageIO.getReaderFormatNames()[0], bos);
            differenceBytes = bos.toByteArray();
        }

        double percentage = mask != null ? difference.calculateDiff(mask) : difference.calculateDiff();

        return new Difference(percentage, differenceBytes);
    }

}
