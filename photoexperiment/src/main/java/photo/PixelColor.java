package photo;

public class PixelColor
{
    int r, g, b;

    @Override
    public String toString()
    {
        return "photo.PixelColor{" +
                "r=" + r +
                ", g=" + g +
                ", b=" + b +
                '}';
    }


    public boolean equals(PixelColor o)
    {
        return r == o.r && g == o.g && g == o.g;
    }

    @Override
    public int hashCode()
    {
        int result = r;
        result = 31 * result + g;
        result = 31 * result + b;
        return result;
    }

    PixelColor(int r, int g, int b)
    {
        this.r = r;
        this.g = g;
        this.b = b;
    }

}