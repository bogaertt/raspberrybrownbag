package photo;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;

public class Photo
{
    public BufferedImage image;
    int width;
    int height;

    static PixelColor BLACK = new PixelColor(0, 0, 0);
    static PixelColor WHITE = new PixelColor(255, 255, 255);

    public Photo(BufferedImage image)
    {
        this.image = image;
        height = image.getHeight();
        width = image.getWidth();
    }

    public Photo difference(Photo other)
    {
        BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {

                PixelColor currentPixelOne = getPixelColorOnXY(x, y);
                PixelColor currentPixelTwo = other.getPixelColorOnXY(x, y);

                int diffRed = Math.abs(currentPixelOne.r - currentPixelTwo.r);
                int diffGreen = Math.abs(currentPixelOne.g - currentPixelTwo.g);
                int diffBlue = Math.abs(currentPixelOne.b - currentPixelTwo.b);

                result.getRaster().setPixel(x, y, new int[]{diffRed, diffGreen, diffBlue});
            }
        }
        return new Photo(result);
    }

    /**
     * Geef Pixelcolor van X,Y op een bufferedImage
     */
    public PixelColor getPixelColorOnXY(int x, int y)
    {
        int[] pixel;

        pixel = image.getRaster().getPixel(x, y, new int[3]);
        //System.out.println("x: " + x + "- y: "+y+"  "+ pixel[0]+","+ pixel[1]+","+ pixel[2]);
        return new PixelColor(pixel[0], pixel[1], pixel[2]);

    }

    public Photo grey()
    {
        BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        ColorConvertOp op = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
        result = op.filter(this.image, result);
        return new Photo(result);
    }

    public Photo substract(Photo other)
    {
        BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                PixelColor currentPixelOne = getPixelColorOnXY(x, y);
                PixelColor currentPixelTwo = other.getPixelColorOnXY(x, y);

                int diffRed = Math.max(0, currentPixelOne.r - currentPixelTwo.r);
                int diffGreen = Math.max(0, currentPixelOne.g - currentPixelTwo.g);
                int diffBlue = Math.max(0, currentPixelOne.b - currentPixelTwo.b);

                result.getRaster().setPixel(x, y, new int[]{diffRed, diffGreen, diffBlue});
            }
        }
        return new Photo(result);
    }

    public boolean isMasked(Photo mask, int x, int y)
    {
        return mask.getPixelColorOnXY(x, y).equals(BLACK);

    }

    /**
     * If value is lower then cutoff percentage, make it black, otherwise white Example: light grey will become white.
     * Dark grey will become black
     *
     * @param cutoffpct
     * @return
     */
    public Photo cutoff(int cutoffpct)
    {
        int cutoffpctRgb = 255 * cutoffpct / 100;
        //System.out.println("cutoffpctRgb = " + cutoffpctRgb);

        BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {

                PixelColor currentPixel = getPixelColorOnXY(x, y);
                int cutoffResult = currentPixel.r > cutoffpctRgb ? 255 : 0;
                result.getRaster().setPixel(x, y, new int[]{cutoffResult, cutoffResult, cutoffResult});
            }
        }
        return new Photo(result);
    }

    public Photo mask(Photo mask)
    {
        BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (isMasked(mask, x, y))
                {
                    result.getRaster().setPixel(x, y, new int[]{0, 0, 0});
                }
                else
                {
                    PixelColor pixel = getPixelColorOnXY(x, y);
                    result.getRaster().setPixel(x, y, new int[]{pixel.r, pixel.g, pixel.b});
                }
            }
        }
        return new Photo(result);
    }

    /**
     * @param reference  Referentiefoto
     * @param scopeX     Aantal pixels in de breedte
     * @param scopeY     Aantal pixels in de hoogte
     * @param upperLeftX X van linker bovenhoek van gebied om te kalibreren
     * @param upperLeftY Y van linker bovenhoek van gebied om te kalibreren
     * @return Gekalibreerde versie van de huidige foto
     */
    public Photo calibrate(Photo reference, int scopeX, int scopeY, int upperLeftX, int upperLeftY)
    {
        int calibR = 0;
        int calibG = 0;
        int calibB = 0;
        //10*10 grid for calibration
        for (int y = upperLeftY; y < upperLeftY + scopeY; y++)
        {
            for (int x = upperLeftX; x < upperLeftX + scopeX; x++)
            {
                PixelColor ref = reference.getPixelColorOnXY(x, y);
                PixelColor cur = getPixelColorOnXY(x, y);

                calibR += ref.r - cur.r;
                calibG += ref.g - cur.g;
                calibB += ref.b - cur.b;
            }
        }

        int nbPixels = scopeX * scopeY;
        PixelColor calibrationDifferencePixel = new PixelColor(calibR / nbPixels, calibG / nbPixels, calibB / nbPixels); //current - reference
        System.out.println("calibrationDifferencePixel = " + calibrationDifferencePixel);

        //calibrate
        BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                PixelColor cur = getPixelColorOnXY(x, y);
                int pixelR = Math.max(0,Math.min(255,cur.r + calibrationDifferencePixel.r));
                int pixelG = Math.max(0,Math.min(255,cur.g + calibrationDifferencePixel.g));
                int pixelB = Math.max(0,Math.min(255,cur.b + calibrationDifferencePixel.b));

                result.getRaster().setPixel(x, y, new int[]{pixelR, pixelG, pixelB});
            }
        }
        return new Photo(result);
    }

    /**
     * Count the percentage of white pixels in the non masked area
     *
     * @param mask
     * @return
     */
    public double calculateDiff(Photo mask)
    {
        int nbNonMaskedPixels = 0;
        int nbWhitePixelsInNonMaskedArea = 0;


        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (!isMasked(mask, x, y))
                {
                    if (getPixelColorOnXY(x, y).equals(WHITE))
                    {
                        nbWhitePixelsInNonMaskedArea++;
                    }
                    nbNonMaskedPixels++;
                }
            }
        }
        return 100 * ((double)nbWhitePixelsInNonMaskedArea / nbNonMaskedPixels);
    }

    public double calculateDiff()
    {
        int nbNonMaskedPixels = 0;
        int nbWhitePixelsInNonMaskedArea = 0;


        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (getPixelColorOnXY(x, y).equals(WHITE))
                {
                    nbWhitePixelsInNonMaskedArea++;
                }
                nbNonMaskedPixels++;
            }

        }
        return 100 * ((double)nbWhitePixelsInNonMaskedArea / nbNonMaskedPixels);
    }
}
