import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;

public class PhotoDifferenceExporter {

    private static final String CONNECTION_STRING = "mongodb://brownbag:brownbag@ds031359.mongolab.com:31359/raspberrybrownbag";
    private static final String DATABASE = "raspberrybrownbag";
    private static final String COLLECTION = "webcam-metrics";

    public void export(Date date, Difference difference) throws UnknownHostException {
        MongoClient client = new MongoClient(new MongoClientURI(CONNECTION_STRING));
        DB database = client.getDB(DATABASE);

        DBCollection collection = database.getCollection(COLLECTION);

        BasicDBObject query = new BasicDBObject("date", retainDateOnly(date));
        if (collection.count(query) == 0) {
            BasicDBObject dailyMetric = new BasicDBObject("date", retainDateOnly(date)).append("metrics", new BasicDBList());
            collection.insert(dailyMetric);
        }

        BasicDBObject metric = new BasicDBObject("date", date);
        metric.append("percentage", difference.getPercentage());
        metric.append("diff", difference.getDifferenceBytes());
        BasicDBObject update = new BasicDBObject("$push", new BasicDBObject("metrics", metric));

        collection.update(query, update);

        client.close();
    }

    private Date retainDateOnly(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

}
