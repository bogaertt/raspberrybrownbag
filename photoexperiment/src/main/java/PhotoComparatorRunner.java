import java.io.IOException;
import java.util.Date;

public class PhotoComparatorRunner {

    private String referenceFile;
    private String currentFile;

    public PhotoComparatorRunner(String referenceFile, String currentFile) {
        this.referenceFile = referenceFile;
        this.currentFile = currentFile;
    }

    public void run() throws IOException {
        Difference difference = new PhotoComparator(referenceFile, currentFile).calculateDifference();
        new PhotoDifferenceExporter().export(new Date(), difference);

        System.out.println("Done");
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.out.println("referencefile and currentfile arguments must be provided");
        } else {
            String referenceFile = args[0];
            String currentFile = args[1];

            new PhotoComparatorRunner(referenceFile, currentFile).run();
        }
    }

}
