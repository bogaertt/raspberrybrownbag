/**
 * @since 20/03/14
 */
public class Difference {

    private double percentage;
    private byte[] differenceBytes;

    public Difference(double percentage, byte[] differenceBytes) {
        this.percentage = percentage;
        this.differenceBytes = differenceBytes;
    }

    public double getPercentage() {
        return percentage;
    }

    public byte[] getDifferenceBytes() {
        return differenceBytes;
    }

}
