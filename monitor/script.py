#!/usr/bin/python
import time
import os
import RPi.GPIO as GPIO

fo = open ('results.txt', 'w',1)
listResult =[0,0,0,0]
counter = 0
alert = False
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()
GPIO.setwarnings(True)
GPIO.setup(17, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)

GPIO.setup(8, GPIO.IN)
GPIO.setup(9, GPIO.IN)
GPIO.setup(10, GPIO.IN)
GPIO.setup(11, GPIO.IN)

print "Buttin IO "
print "8: " + str(GPIO.input(8))
print "9: " + str(GPIO.input(9))
print "10: " + str(GPIO.input(10))
print "11: " + str(GPIO.input(11))

while True:
    if (GPIO.input(8) == False):
        listResult[0] = listResult[0]+1
    if (GPIO.input(9) == False):
        listResult[1] = listResult[1]+1
    if (GPIO.input(10) == False):
        listResult[2] = listResult[2]+1
    if (GPIO.input(11) == False):
        listResult[3] = listResult[3]+1

    #    GPIO.output(17, GPIO.HIGH)
    #    GPIO.output(27, GPIO.LOW)
        
    #else:
    #    GPIO.output(17, GPIO.LOW)
    #    GPIO.output(27, GPIO.HIGH)
        #print GPIO.input(10)
    counter = counter +1

    if (counter % 10 == 0 ) :
        if (counter % 20 == 0 and alert):
            GPIO.output(23, GPIO.HIGH)
        else :
            GPIO.output(23, GPIO.LOW)
            
    time.sleep(.01)
    if (counter % 100 == 0) :
        for i in listResult :
            print(str(i) + '/'+str(counter))
        #fo.write(str(sum(listResult)) + '/'+str(counter*4)+ '\n')
        avg = (sum(listResult) * 100 // (counter * len(listResult)))
        GPIO.output(17, GPIO.LOW)
        GPIO.output(22, GPIO.LOW)
        GPIO.output(27, GPIO.LOW)
        alert = False;
        if(avg > 5):
            GPIO.output(17, GPIO.HIGH)
        if(avg > 30):
            GPIO.output(27, GPIO.HIGH)
        if(avg > 50):
            GPIO.output(22, GPIO.HIGH)
        if(avg > 75):
            alert = True
    if (counter % 100 == 0) :
        fo = open ('results.txt', 'w',1)
        fo.write('{"total":'+ str((sum(listResult) * 100 // (counter * len(listResult))))
                 +',"detail":'
                 +'['+str(listResult[0]*100//counter)
                 +','+str(listResult[1]*100//counter)
                 +','+str(listResult[2]*100//counter)
                 +','+str(listResult[3]*100//counter)
                 +']'
                 +'}')
        fo.close()
        os.system('cp results.txt live.json')
        counter = 0
        listResult[0] = 0
        listResult[1] = 0
        listResult[2] = 0
        listResult[3] = 0
